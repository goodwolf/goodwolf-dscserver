#
# Cookbook Name:: dscserver
# Recipe:: server
#
# Copyright (c) 2015 Connor Goodwolf

include_recipe 'java'

directory '/opt/dscserver' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

remote_file '/opt/dscserver/DscServer.jar' do
  source 'https://sites.google.com/site/mppsuite/downloads/dscserver-2/DscServer.jar?attredirects=0&d=1'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

user 'dscserver' do
  comment 'dscserver user'
  home '/home/dscserver'
  manage_home true
  system true
  action :create
end

directory '/home/dscserver/.java/.userPrefs/mpp/dsc/server/DscServer/' do
  owner 'dscserver'
  group 'dscserver'
  mode 0755
  recursive true
  action :create
end

execute "chown /home/dscserver/.java" do
  command "chown -R dscserver:dscserver /home/dscserver/.java"
  only_if { Etc.getpwuid(File.stat('/home/dscserver/.java').uid).name != "dscserver" }
end

template '/home/dscserver/.java/.userPrefs/mpp/dsc/server/DscServer/prefs.xml' do
  owner 'dscserver'
  group 'dscserver'
  source "prefs.xml.erb"
  variables(
      :server => node['dscserver']['server'],
      :password => node['dscserver']['password']
  )
end

cookbook_file '/lib/systemd/system/dscserver.service' do
  source 'dscserver.service'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
  only_if { node['platform'] == 'ubuntu' and node['platform_version'] == '15.04' }
end

execute 'enable dscserver at boot' do
  command 'systemctl enable dscserver'
  only_if { node['platform'] == 'ubuntu' and node['platform_version'] == '15.04' }
end

